<?php
/**
 * @file
 * Admin form callbacks for develop mode module.
 */

/**
 * Administration settings form.
 *
 * @see system_settings_form()
 */
function develop_mode_settings_form() {
  $form['develop_mode_block_robots'] = array(
    '#type' => 'checkbox',
    '#title' => t('Block search engine indexing'),
    '#default_value' => variable_get('develop_mode_block_robots', FALSE),
  );
  $form['develop_mode_force_access_login'] = array(
    '#type' => 'checkbox',
    '#title' => t('Block unauthenticated user browsing'),
    '#default_value' => variable_get('develop_mode_force_access_login', FALSE),
  );
  $form['credentials'] = array(
    '#type' => 'fieldset',
    '#states' => array(
      'visible' => array(
        ':input[name="develop_mode_force_access_login"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['credentials']['develop_mode_force_access_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Anonymous Access Username'),
    '#size' => 15,
    '#maxlength' => 32,

  );
  $form['credentials']['develop_mode_force_access_pass'] = array(
    '#type' => 'password_confirm',
    '#title' => t('Anonymous Access Password'),
    '#size' => 32,
  );
  $form = system_settings_form($form);
  return $form;
}
