<?php
/**
 * @file
 * User form callbacks for develop mode module.
 */

/**
 * Login form for browsing as anonymous user.
 */
function develop_mode_login_form() {
  $form['user'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#size' => 15,
    '#maxlength' => 32,
    '#required' => TRUE,
  );
  $form['pass'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#maxlength' => 32,
    '#size' => 15,
    '#required' => TRUE,
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Authenticate'),
  );
  return $form;
}
